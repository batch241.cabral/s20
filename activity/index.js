let num = Number(prompt("Enter a number: "));
console.log("The number you provided is " + num + ".");
for (let i = num; i >= 0; i--) {
    if (i % 10 === 0 && i >= 55) {
        console.log("The number is divisible by 10. Skipping this number.");
    } else if (i % 10 === 5) {
        console.log(i);
    } else if (i <= 50) {
        console.log("The current value is at 50. Terminating the loop.");
        break;
    }
}

let string = "supercalifragilisticexpialidocious";
let storeStringHere = "";

for (let i = 0; i < string.length; i++) {
    let condition = string.charAt(i).toUpperCase();
    if (condition >= "A" && condition <= "Z") {
        if (
            condition !== "A" &&
            condition !== "E" &&
            condition !== "I" &&
            condition !== "O" &&
            condition !== "U"
        ) {
            storeStringHere += string.charAt(i);
        }
    }
}
console.log(string);
console.log(storeStringHere.toLocaleLowerCase());
